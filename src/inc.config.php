<?php
/**
 * @license Apache 2.0
 */

namespace DataSearchEngine;

/**
 * Search engine properties file
 *
 * @package DataSearchEngine
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */

// General properties
define('ADMIN_NAME',    'Xavier MADIOT');
define('ADMIN_MAIL',    'x.madiot@girondenumerique.fr');

// Environment properties
define('DEVELOPMENT',   true);
define('PRODUCTION',    false);
define('DEBUG',         true);
define('LOGGING',       true);

// Solr server properties
define('SOLR_SERVER',   '*****************************');
define('SOLR_USER',     'solr');
define('SOLR_PASSWORD', '*****');
define('SOLR_PORT',     8983);
define('SOLR_CORE',     'documents');

// OpenLDAP server properties
define('LDAP_SERVER',   '****************************');
define('LDAP_PORT',     '389');

// INSEE Sirene API properties
define('SIRENE_KEY',    '****************************');
define('SIRENE_SECRET', '****************************');