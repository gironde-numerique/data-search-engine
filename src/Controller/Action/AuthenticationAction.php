<?php
/**
 * @license Apache 2.0
 */

namespace DataSearchEngine\Controller\Action;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use DataSearchEngine\Utils\ArrayUtils;
use DataSearchEngine\Entity\User;
use DataSearchEngine\Entity\Enum\Rank;

/**
 * Login action.
 *
 * @package DataSearchEngine\Action
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
final class AuthenticationAction extends ActionController {

     public function __construct(ContainerInterface $container) {
          parent::__construct($container);
     }

     public function login(ServerRequestInterface $request, ResponseInterface $response) : ResponseInterface {
          $params = $request->getParsedBody();
          if ($params != null && sizeof($params) > 0) {
               $email         = strtolower(ArrayUtils::get($params, 'email'));
               $password      = ArrayUtils::get($params, 'password');

               $emailArray = explode('@', $email);
               $login = $emailArray[0];
               $domainArray = explode('.', $emailArray[1]);
               $domain = $domainArray[0];
               $tld = '';
               if (sizeof($domainArray) > 1) {
                    $tld = '.'.$domainArray[1];
               }
               
               $ldapDn = sprintf('uid=%s,ou=users,o=%s%s,dc=global,dc=gn', $login, $domain, $tld);
               $ldapConnection = @ldap_connect(LDAP_SERVER, LDAP_PORT);
               ldap_set_option($ldapConnection, LDAP_OPT_PROTOCOL_VERSION, 3);			// Force LDAPv3 (for openldap)
               if (@ldap_bind($ldapConnection, $ldapDn, $password)) {
                    // Get user informations
                    $search = ldap_search($ldapConnection, sprintf('ou=users,o=%s%s,dc=global,dc=gn', $domain, $tld), 'uid='.$login);
                    if (ldap_count_entries($ldapConnection, $search) == 1) {
                         $informations = ldap_get_entries($ldapConnection, $search);

                         // If rank 1 or 2, is Admin, else check the description to know if this user is available to access to this application
                         $rank = $informations[0]['employeetype'][0];
                         if ( $rank == 1 || $rank == 2 
                              || strpos($informations[0]['description'][0], 'DATASEARCHENGINE') !== false ) {
                              
                              $displayName = explode(' ', $informations[0]['displayname'][0]);

                              $user = new User();
                              $user->setFirstname(end($displayName));
                              $user->setLastname(strtoupper($displayName[0]));
                              $user->setEmail($informations[0]['mail'][0]);
                              if( $rank == 1 || $rank == 2) {
                                   $user->setRank(Rank::ADMIN());
                              } else {
                                   $user->setRank(Rank::AGENT());
                              }
                              $this->session->set('user', \serialize($user));
                              
                              return $response->withHeader('Location', '/administration');
                         } else {
                              $this->flash->addMessage('error', 'Droit insuffisant pour acc&egrave;der &agrave; cette page');
                         }

                    } else {
                         $this->flash->addMessage('error', 'Utilisateur inconnu');
                    }
                    @ldap_unbind($ldapConnection);
                    @ldap_close($ldapConnection);
               } else {
                    $this->flash->addMessage('error', 'Adresse email ou mot de passe erron&eacute;');
               }
          }
          return $response->withHeader('Location', '/administration');
     }

     public function logout(ServerRequestInterface $request, ResponseInterface $response) : ResponseInterface {
          $this->session->clear();
		$this->session->save();
          
          return $response->withHeader('Location', '/administration');
     }

}