<?php
/**
 * @license Apache 2.0
 */

namespace DataSearchEngine\Controller\Action;

use DateTime;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use DataSearchEngine\Utils\ArrayUtils;
use DataSearchEngine\Utils\NormalizeString;
use DataSearchEngine\Entity\CitizenDocument;
use DataSearchEngine\Entity\DocumentFile;
use DataSearchEngine\Entity\SearchCriteria;

/**
 * Administration action.
 *
 * @package DataSearchEngine\Action
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
final class AdministrationAction extends ActionController {

	public function __construct(ContainerInterface $container) {
		parent::__construct($container);
	}

	 public function search(ServerRequestInterface $request, ResponseInterface $response) : ResponseInterface {
		$total = 0;
		$params = $request->getParsedBody();
		$response->withHeader('Content-Type', 'application/json');
		if ($params != null && sizeof($params) > 0) {
			$filename      = ArrayUtils::get($params, 'filename');
			$date          = ArrayUtils::get($params, 'date');
			$siren         = ArrayUtils::get($params, 'siren');
			$offset        = ArrayUtils::get($params, 'offset');
			$limit         = ArrayUtils::get($params, 'limit');

			$solr = $this->container->get('solr');
			try {
				// If filename is an URL, get only filename
				if (strpos($filename, 'http://') !== false || strpos($filename, 'https://') !== false) {
					$filename = basename($filename);
				}
				// Plain text search.
				$criteria = new SearchCriteria(null, $filename, null, $siren);
				$criteria->setDate($date);
				$search = $solr->adminSearch($criteria, $offset, $limit);
				$total = $search['response']['numFound'];
				$response->getBody()->write(json_encode($search['response']));
				if ($total == 0) {
					return $response->withStatus(204);
				} else if (sizeof($search['response']['docs']) < $total) {
					return $response->withStatus(206);
				} else {
					return $response->withStatus(200);
				}
				
			} catch (\Exception $e) {
				$response->getBody()->write($e->getMessage());
				return $response->withStatus(500);
			}
		}
	}

	public function purge(ServerRequestInterface $request, ResponseInterface $response) : ResponseInterface {
		$params = $request->getParsedBody();
		if ($params != null && sizeof($params) > 0) {
			$origin = ArrayUtils::get($params, 'origin');
			$siren  = ArrayUtils::get($params, 'siren');
			$total  = ArrayUtils::get($params, 'total');
		
			$solr = $this->container->get('solr');
			if (!empty($origin) && !empty($siren)) {
				if (strcmp($origin, 'tous') !== 0) {
					$search = $solr->adminSearchByOrigin($siren, $total, $origin);
				} else {
					$search = $solr->adminSearchByOrigin($siren, $total);
				}
				$deletedDocuments   = 0;
				$pathRoot           = '/OpenData';
				foreach ($search['response']['docs'] as $file) {
						// File is local ?
						if (strpos($file->filepath[0], $_SERVER['HTTP_HOST']) !== false) {
							$path = substr($file->filepath[0], strpos($file->filepath[0], $pathRoot) + strlen($pathRoot) + 1);
							// File exist ?
							if (file_exists(DIR_ROOT.$pathRoot.'/'.$path)) {
								// Try to delete the document
								if (unlink(DIR_ROOT.$pathRoot.'/'.$path)) {
									// Try to delete index of the document
									if ($solr->deleteDocumentIndexById($file->id)) {
										$deletedDocuments++;
									}
								} 
							}
						}
				}

				if ($total == 0) {
					$this->flash->addMessage('warning', 'Aucun document &agrave; supprimer dans cette cat&eacute;gorie');
				} else if ($deletedDocuments > 0 && $deletedDocuments == $total) {
					$message = 'Purge effectu&eacute;e !<br />';
					if ($deletedDocuments > 1) {
						$message .= $deletedDocuments.' documents ont &eacute;t&eacute; supprim&eacute;s';
					} else {
						$message .= '1 document a &eacute;t&eacute; supprim&eacute;';
					}
					$this->flash->addMessage('success', $message);
				} else if ($deletedDocuments > 0 && $deletedDocuments < $total) {
					$message = 'Certains documents n\'ont pas pu &ecirc;tre supprim&eacute; ! <br /> ';
					if ($deletedDocuments > 1) {
						$message .= $deletedDocuments.' documents ont &eacute;t&eacute; supprim&eacute;s';
					} else {
						$message .= '1 document a &eacute;t&eacute; supprim&eacute;';
					}
					$message .= ' sur un total de '.$total.' document(s)';

					$this->flash->addMessage('warning', $message);
				} else if ($deletedDocuments == 0) {
					$this->flash->addMessage('error', 'Une erreur s\'est produite lors de la suppression des documents');
				}
			} else {
				$this->flash->addMessage('error', 'Veuillez choisir une cat&eacute;gorie de document &agrave; supprimer');
			}
			return $response->withHeader('Location', '/administration');
		}
	}

	public function upload(ServerRequestInterface $request, ResponseInterface $response) : ResponseInterface {
		$params = $request->getParsedBody();
		$response->withHeader('Content-Type', 'application/json');
		if ($params != null && sizeof($params) > 0) {
			$siren         	= ArrayUtils::get($params, 'siren');
			$category      	= ArrayUtils::get($params, 'category');
			$date      		= DateTime::createFromFormat('Y-m-d', ArrayUtils::get($params, 'date'));
			$description   	= ArrayUtils::get($params, 'description');

			$sirene   = $this->container->get('sirene');
			$solr     = $this->container->get('solr');
			try {
				if ($this->session->has('collectivite')) {
					$collectivite = unserialize($this->session->get('collectivite'));
				} else {
					$collectivite = $sirene->getOrganizationInformations($siren);
					if ($collectivite != null) {
						$this->session->set('collectivite', \serialize($collectivite));
					}
				}
			} catch (\Exception $e) {
				$response->getBody()->write($e->getMessage());
				return $response->withStatus(500);
			}

			if ($collectivite != null) {
				$destinationDirectory = '/OpenData/'.$this->getDestinationDirectory($category).'/'.$date->format('Y');
				if (!is_dir(DIR_ROOT.$destinationDirectory)) {
					mkdir(DIR_ROOT.$destinationDirectory, 0755);
				}
				$filename = NormalizeString::normalize($_FILES['file']['name']);
				if (NormalizeString::checkFormat($filename)) {
					$filepath = $destinationDirectory.'/'.$filename;
					if (move_uploaded_file($_FILES['file']['tmp_name'], DIR_ROOT.$filepath)) {
						$files = array();
						$documentFile = new DocumentFile($filename, DIR_ROOT.$filepath);
						$documentFile->setOpenDataUrl('https://'.$_SERVER['SERVER_NAME'].$filepath);
						array_push($files, $documentFile);
						$document = new CitizenDocument($description, $destinationDirectory, $files);
						$document->setDate($date);
						$document->setOrigin('publication_administration');
						$document->setCollectivite($collectivite);
	
						// Document indexation on Solr server
						try {
							if (!$solr->documentIsAlreadyIndexed($document->getCollectivite()->getSiren(), $documentFile->getHash())) {
								if ($solr->indexDocumentFile($document, $documentFile)) {
									$response->getBody()->rewind();
									$response->getBody()->write('Publication effectu&eacute;e');
									return $response->withStatus(200);
								} else {
									unlink(DIR_ROOT.$filepath);
									$response->getBody()->write('Le fichier n\a pu &ecirc;tre index&eacute;');
									return $response->withStatus(500);
								}
							} else {
								unlink(DIR_ROOT.$filepath);
								$response->getBody()->write('Ce fichier a d&eacute;j&agrave; &eacute;t&eacute; publi&eacute;');
								return $response->withStatus(200);
							}
						} catch (\Exception $e) {
							$response->getBody()->write($e->getMessage());
							return $response->withStatus(500);
						}
					} else {
						$response->getBody()->write('Erreur lors du t&eacute;l&eacute;chargement du fichier');
						return $response->withStatus(500);
					}
				} else {
					$response->getBody()->write('Le format du document n\'est pas autoris&eacute;, seul les documents de type .pdf, .doc(x), .csv, .json, .txt, .xml, .ppt(x), xls(x), .odp, .ods, .odt, .rtf sont permis');
					return $response->withStatus(400);
				}
			} else {
				$response->getBody()->write('Impossible de r&eacute;cup&eacute;rer les informations de la collectivit&eacute; pour le SIREN '.$siren);
				return $response->withStatus(500);
			}
		} else {
			$response->getBody()->write('Les param&eacute;tres saisis ne permettent pas de satisfaire la requ&ecirc;te');
			return $response->withStatus(400);
		}
	}

	public function delete(ServerRequestInterface $request, ResponseInterface $response) : ResponseInterface {
		$params = $request->getParsedBody();
		if ($params != null && sizeof($params) > 0) {
			$documentUrl   = ArrayUtils::get($params, 'documentUrl');
			$documentId    = ArrayUtils::get($params, 'documentId');
			
			if (strpos($documentUrl, $_SERVER['HTTP_HOST']) !== false) {
				$path = substr($documentUrl, strpos($documentUrl, '/OpenData') + 10);
				if (file_exists(DIR_ROOT.'/OpenData//'.$path)) {
					if (unlink(DIR_ROOT.'/OpenData//'.$path)) {
						return $this->deleteDocumentIndex($response, $documentId);
					} else {
						$response->getBody()->write('Une erreur est survenue lors de la suppression du document');
						return $response->withStatus(500);
					}
				} else {
					$message = 'Le document '.$path.' n\'a pas &eacute;t&eacute; trouv&eacute; sur le serveur.<br />';
					return $this->deleteDocumentIndex($response, $documentId, $message);
				}
			} else {
				$response->getBody()->write('Le document n\'est pas disponible sur ce serveur.<br />');
				return $response->withStatus(404);
			}
		}
	}

	/**
	 * Call SolrConsumer to delete a document index
	 *
	 * @param ResponseInterface $response Response object from Slim routing
	 * @param string $documentId Solr document id
	 * @param string $message (optional) Optional message
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return ResponseInterface
	 * @access private
	 */
	private function deleteDocumentIndex(ResponseInterface $response, string $documentId, $message = '') : ResponseInterface {
		$solr = $this->container->get('solr');
		try {
			if ($solr->deleteDocumentIndexById($documentId)) {
				$response->getBody()->write($message.'Le document a bien &eacute;t&eacute; supprim&eacute;');
				return $response->withStatus(200);
			} else {
				$response->getBody()->write('Une erreur est survenue lors de la suppression de l\'index du document');
				return $response->withStatus(500);
			}        
		} catch (\Exception $e) {
			$response->getBody()->write($e->getMessage());
			return $response->withStatus(500);
		}
	}
}