<?php
/**
 * @license Apache 2.0
 */

namespace DataSearchEngine\Controller\Action;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Exception\HttpBadRequestException;
use Slim\Exception\HttpForbiddenException;
use DataSearchEngine\Entity\SearchCriteria;
use DataSearchEngine\Utils\ArrayUtils;
use DataSearchEngine\Utils\StringUtils;

/**
 * Public action, to do simple search, advanced search and files exploring.
 *
 * @package DataSearchEngine\Action
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
class PublicAction extends ActionController {

	const OPENDATA_DIRECTORY = 'OpenData';

	public function __construct(ContainerInterface $container) {
		parent::__construct($container);
	}

	public function search(ServerRequestInterface $request, ResponseInterface $response) : ResponseInterface {
		$total = 0;
		$params = $request->getParsedBody();
		$response->withHeader('Content-Type', 'application/json');
		if ($params != null && sizeof($params) > 0) {
			$keywords  = ArrayUtils::get($params, 'search-field');
			$category  = ArrayUtils::get($params, 'category');
			$siren     = ArrayUtils::get($params, 'siren');
			$offset    = ArrayUtils::get($params, 'offset');
			$limit     = ArrayUtils::get($params, 'limit');

			$solr = $this->container->get('solr');

			// Plain text search.
			$criteria = new SearchCriteria($keywords, $this->getDestinationDirectory($category), null, $siren);
			$search = $solr->simpleSearch($criteria, $offset, $limit);
			$total = $search['response']['numFound'];
			if ($total == 0) {
				// Seconde chance with query filter search.
				$search = $solr->simpleSearch($criteria, $offset, $limit, true);
				$total = $search['response']['numFound'];
			}

			$response->getBody()->write(json_encode($search['response']));
		}
		
		if ($total == 0) {
			return $response->withStatus(204);
		} else if (sizeof($search['response']['docs']) < $total) {
			return $response->withStatus(206);
		} else {
			return $response->withStatus(200);
		}
	}

	public function advancedSearch(ServerRequestInterface $request, ResponseInterface $response) : ResponseInterface {
		$total = 0;
		$params = $request->getParsedBody();
		$response->withHeader('Content-Type', 'application/json');
		if ($params != null && sizeof($params) > 0) {
			$keywords  = ArrayUtils::get($params, 'content');
			$siren     = ArrayUtils::get($params, 'siren');
			$category  = ArrayUtils::get($params, 'category');
			$type      = ArrayUtils::get($params, 'type');

			// Careful, keep this encoding for special chars !
			if (!empty($type)) {
				switch ($type) {
					case 1 :
						$type = 'Actes rÃ©glementaires';
						break;
					case 2 :
						$type = 'DÃ©libÃ©rations';
						break;
					case 3 :
						$type = 'Documents budgÃ©taires et financiers';
						break;
					default :
						$type = null;
				}
			}
			$startdate = ArrayUtils::get($params, 'startdate');
			$enddate   = ArrayUtils::get($params, 'enddate');
			$zipcode   = ArrayUtils::get($params, 'zipcode');
			$city      = ArrayUtils::get($params, 'city');
			$offset    = ArrayUtils::get($params, 'offset');
			$limit     = ArrayUtils::get($params, 'limit');

			$solr = $this->container->get('solr');

			// Plain text search.
			$criteria = new SearchCriteria($keywords, $this->getDestinationDirectory($category), $type, $siren);
			$criteria->setStartdate($startdate);
			$criteria->setEnddate($enddate);
			$criteria->setZipcode($zipcode);
			$criteria->setCity($city);
			$search = $solr->advancedSearch($criteria, $offset, $limit);
			$total = $search['response']['numFound'];
			if ($total == 0) {
				// Seconde chance with query filter search.
				$search = $solr->advancedSearch($criteria, $offset, $limit, true);
				$total = $search['response']['numFound'];
			}

			$response->getBody()->write(json_encode($search['response']));
		}
		
		if ($total == 0) {
			return $response->withStatus(204);
		} else if (sizeof($search['response']['docs']) < $total) {
			return $response->withStatus(206);
		} else {
			return $response->withStatus(200);
		}
	}

	public function explore(ServerRequestInterface $request, ResponseInterface $response, $args) : ResponseInterface {
		$nbFile = 0;
		$params = $request->getParsedBody();
		if ($params != null && sizeof($params) > 0) {
			$directory  = ArrayUtils::get($params, 'directory');
			$category  	= ArrayUtils::get($params, 'category');

			if (StringUtils::startsWith($directory, self::OPENDATA_DIRECTORY)) {
				if (strcmp($directory, self::OPENDATA_DIRECTORY) === 0 && $category != null) {
					$directory .= '/'.$this->getDestinationDirectory($category);
				}
				$path = explode('/', $directory);
				$tempPath = '';
				$level = 0;
				$pathLength = count($path);
	
				$page = '<nav id="breadcrumb" aria-label="breadcrumb" data-toggle="collapse" data-target="#documents-area" aria-expanded="true" aria-controls="documents">
							<ol class="breadcrumb">';
	
				foreach ($path as $stepFolder) {
					if ($stepFolder !== '') { $tempPath .= $stepFolder; }
					if ($stepFolder !== '..') {
						if ($level++ == $pathLength - 1) {
							$page .= '<li class="breadcrumb-item active" aria-current="page">'.self::getDirectoryUsualName($stepFolder).'</li>';
						} else {
							$page .= '<li class="breadcrumb-item"><a href="'.$tempPath.'">'.self::getDirectoryUsualName($stepFolder).'</a></li>';
						}
					}
					if ($level < $pathLength) { $tempPath .= '/'; }
				}			
				$page .= '</ol>
							<i class="fas fa-chevron-down"></i>
						</nav>';
						
				if (is_dir($directory)) {
					if ($folder = opendir($directory)) {
						$tabFiles = array();
						while (FALSE !== ($file = readdir($folder))) {
							if ($file != '.' && $file != '..') {
								$nbFile ++;
								array_push($tabFiles, $file);
							}
						}
						$page .= '<div class="collapse show" id="documents-area">';
						if ($nbFile == 0) { 
							$page .= '<strong class="font-italic">Ce répertoire est vide.</strong>';
						} else {
							$page .= '<ul id="documents">';
							// Sort folder or documents by level
							if ($level == 1) {
								// Sort by folder asc
								sort($tabFiles);
							} else if ($level == 2) {
								// Sort by date desc
								rsort($tabFiles);
							} else {
								// Sort by creation date asc
								$documentsArray = array();
								foreach ($tabFiles as $file) {
									$filename = explode('_', $file);
									$documentsArray[end($filename)] = $file;
								}
								
								krsort($documentsArray);
								$tabFiles = array_values($documentsArray);
							}
							
							foreach ($tabFiles as $file) {
								if (is_dir($directory.'/'.$file)) {
									// Folder
									$page .= '<li class="folder"><a class="link mime folder" href="'.$directory.'/'.$file.'">'.self::getDirectoryUsualName($file).'</a></li>';
								} else if ($file[0] != '.') {
									// File
									$page .= '<li class="file"><a class="link mime '.self::getMimeType($file).'" target="_blank" href="'.$directory.'/'.$file.'">'.$file.'</a></li>';
								}
							}
							$page .= '</ul>';
						}
						$page .= '</div><br />';
					
						closedir($folder);
						$response->getBody()->write($page);      
					} else {
						$response->getBody()->write('Le dossier /'.$directory.' n\'a pu &ecirc;tre ouvert.');
						return $response->withStatus(500);
					}
				} else {
					$response->getBody()->write('Le dossier /'.$directory.' n\'existe pas.');
					return $response->withStatus(404);
				}

				return $response;
			} else {
				throw new HttpForbiddenException($request);
			}
		} else {
			throw new HttpBadRequestException($request);
		}
	}

	/**
	 * Get usual name for a specific directory.
	 *
	 * @param string $directory Directory name from filesystem
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return string
	 * @access private
	 */
	private static function getDirectoryUsualName(string $directory) : string {
		switch ($directory) {
			case '0_Actes_administratifs':
				$directoryName = 'Actes administratifs'; 
				break;
			case '1_Commande_publique':
				$directoryName = 'Commande publique'; 
				break;
			case '2_Urbanisme':
				$directoryName = 'Urbanisme'; 
				break;
			case '3_Domaine_et_patrimoine':
				$directoryName = 'Domaine et patrimoine'; 
				break;
			case '4_Finances_locales':
				$directoryName = 'Finances locales'; 
				break;
			case '5_Autres_domaines_de_competences':
				$directoryName = 'Autres domaines de comp&eacute;tences';
				break;
			default:
				$directoryName = ucfirst($directory);
		}

		return $directoryName;
	}

	/**
	 * Get mime type from filename
	 * 
	 * @param string $filename File name
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return string
	 * @access private
	 */
	private static function getMimeType(string $filename) : string {
		$extension = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
		if (!(strpos($extension, '.') !== false)) {
			$extension = '.'.$extension;
		}
		switch ($extension) {
			case '.aac':    // AAC audio
				$mime ='audio'; break; 
			case '.abw':    // AbiWord document
				$mime ='base'; break; 
			case '.arc':    // Archive document
				$mime ='base'; break; 
			case '.avi':    // AVI: Audio Video Interleave
				$mime ='video'; break; 
			case '.azw':    // Amazon Kindle eBook format
				$mime ='base'; break; 
			case '.bin':    // Any kind of binary data
				$mime ='base'; break; 
			case '.bmp':    // Windows OS/2 Bitmap Graphics
				$mime ='image'; break; 
			case '.bz':     // BZip archive
				$mime ='base'; break; 
			case '.bz2':    // BZip2 archive
				$mime ='base'; break; 
			case '.csh':    // C-Shell script
				$mime ='base'; break; 
			case '.css':    // Cascading Style Sheets (CSS)
				$mime ='base'; break; 
			case '.csv':    // Comma-separated values (CSV)
				$mime ='csv'; break; 
			case '.doc':    // Microsoft Word
				$mime ='doc'; break; 
			case '.docx':   // Microsoft Word (OpenXML)
				$mime ='docx'; break; 
			case '.eot':    // MS Embedded OpenType fonts
				$mime ='base'; break; 
			case '.epub':   // Electronic publication (EPUB)
				$mime ='base'; break; 
			case '.gif':    // Graphics Interchange Format (GIF)
				$mime ='image'; break; 
			case '.htm':    // HyperText Markup Language (HTML)
				$mime ='html'; break; 
			case '.html':   // HyperText Markup Language (HTML)
				$mime ='html'; break; 
			case '.ico':    // Icon format
				$mime ='image'; break; 
			case '.ics':    // iCalendar format
				$mime ='base'; break; 
			case '.jar':    // Java Archive (JAR)
				$mime ='base'; break; 
			case '.jpeg':   // JPEG images
				$mime ='image'; break;
			case '.jpg':    // JPEG images
				$mime ='image'; break; 
			case '.js':     // JavaScript (IANA Specification) (RFC 4329 Section 8.2)
				$mime ='base'; break; 
			case '.json':   // JSON format
				$mime ='json'; break; 
			case '.mid':    // Musical Instrument Digital Interface (MIDI)
				$mime ='audio'; break; 
			case '.midi':   // Musical Instrument Digital Interface (MIDI)
				$mime ='audio'; break; 
			case '.mpeg':   // MPEG Video
				$mime ='video'; break; 
			case '.mpkg':   // Apple Installer Package
				$mime ='base'; break; 
			case '.odp':    // OpenDocument presentation document
				$mime ='odp'; break; 
			case '.ods':    // OpenDocument spreadsheet document
				$mime ='ods'; break; 
			case '.odt':    // OpenDocument text document
				$mime ='odt'; break; 
			case '.oga':    // OGG audio
				$mime ='audio'; break; 
			case '.ogv':    // OGG video
				$mime ='video'; break; 
			case '.ogx':    // OGG
				$mime ='base'; break; 
			case '.otf':    // OpenType font
				$mime ='base'; break; 
			case '.png':    // Portable Network Graphics
				$mime ='image'; break; 
			case '.pdf':    // Adobe Portable Document Format (PDF)
				$mime ='pdf'; break; 
			case '.ppt':    // Microsoft PowerPoint
				$mime ='ppt'; break; 
			case '.pptx':   // Microsoft PowerPoint (OpenXML)
				$mime ='pptx'; break; 
			case '.rar':    // RAR archive
				$mime ='base'; break; 
			case '.rtf':    // Rich Text Format (RTF)
				$mime ='rtf'; break; 
			case '.sh':     // Bourne shell script
				$mime ='base'; break; 
			case '.svg':    // Scalable Vector Graphics (SVG)
				$mime ='image'; break; 
			case '.swf':    // Small web format (SWF) or Adobe Flash document
				$mime ='base'; break; 
			case '.tar':    // Tape Archive (TAR)
				$mime ='base'; break; 
			case '.tif':    // Tagged Image File Format (TIFF)
				$mime ='image'; break; 
			case '.tiff':   // Tagged Image File Format (TIFF)
				$mime ='image'; break; 
			case '.ts':     // Typescript file
				$mime ='base'; break; 
			case '.ttf':    // TrueType Font
				$mime ='base'; break; 
			case '.txt':    // Text, (generally ASCII or ISO 8859-n)
				$mime ='txt'; break; 
			case '.vsd':    // Microsoft Visio
				$mime ='vsd'; break; 
			case '.wav':    // Waveform Audio Format
				$mime ='audio'; break; 
			case '.weba':   // WEBM audio
				$mime ='audio'; break; 
			case '.webm':   // WEBM video
				$mime ='video'; break; 
			case '.webp':   // WEBP image
				$mime ='image'; break; 
			case '.woff':   // Web Open Font Format (WOFF)
				$mime ='base'; break; 
			case '.woff2':  // Web Open Font Format (WOFF)
				$mime ='base'; break; 
			case '.xhtml':  // XHTML
				$mime ='html'; break; 
			case '.xls':    // Microsoft Excel
				$mime ='xls'; break; 
			case '.xlsx':   // Microsoft Excel (OpenXML)
				$mime ='xlsx'; break; 
			case '.xml':    // XML
				$mime ='xml'; break; 
			case '.xul':    // XUL
				$mime ='base'; break; 
			case '.zip':    // ZIP archive
				$mime ='base'; break; 
			case '.3gp':    // 3GPP audio/video container
				$mime ='video'; break; 
			case '.3g2':    // 3GPP2 audio/video container 
				$mime ='video'; break; 
			case '.7z':     // 7-zip archive
				$mime ='base'; break; 
			default:        // general purpose MIME-type
				$mime = 'base'; 
		}

		return $mime;
	}

}
