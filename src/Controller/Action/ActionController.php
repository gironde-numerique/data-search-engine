<?php
/**
 * @license Apache 2.0
 */

namespace DataSearchEngine\Controller\Action;

use Psr\Container\ContainerInterface;

/**
 * ActionController interface class to load commons object for action controllers.
 *
 * @package DataSearchEngine\Controller\Action
 * @author  Alexis ZUCHER <a.zucher@girondenumerique.fr>
*/
class ActionController {

    protected $container;

    protected $flash;

    protected $user;

	protected $session;

    public function __construct(ContainerInterface $container) {
        // Slim container
        $this->container = $container;

        // Flash messages
        $this->flash = $this->container->get('flash');

        // Authenticated user
        $this->user = $this->container->get('user');

		// Session
		$this->session = $this->container->get('session');
    }

    /**
	 * Get destination directory from id.
	 *
	 * @param string $category Category id as string
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return string
	 * @access protected
	 */
	protected function getDestinationDirectory(?string $category) : string {
		switch ($category) {
			case '0':
				$destinationDirectory = '0_Actes_administratifs';
				break;
			case '1':
				$destinationDirectory = '1_Commande_publique';
				break;
			case '2':
				$destinationDirectory = '2_Urbanisme';
				break;
			case '3':
				$destinationDirectory = '3_Domaine_et_patrimoine';
				break;
			case '4':
				$destinationDirectory = '4_Finances_locales';
				break;
			case '5':
				$destinationDirectory = '5_Autres_domaines_de_competences';
				break;
			default :
				$destinationDirectory = '';
		}

		return $destinationDirectory;
	}
}