<?php
/**
 * @license Apache 2.0
 */

namespace DataSearchEngine\Controller\View;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use DataSearchEngine\Utils\ArrayUtils;

/**
 * Class PublicViewController to set public GUI part.
 *
 * @package DataSearchEngine\Controller\View
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
*/
final class PublicViewController extends ViewController {

    public function __construct(ContainerInterface $container) {
        parent::__construct($container);
    }

    public function classicView(ServerRequestInterface $request, ResponseInterface $response, $args) : ResponseInterface {
        return $this->twig->render($response, 'accueil.html', [
            'currentUrl'        => $request->getUri()->getPath()
        ]);
    }

    public function withDocumentTypeFilter(ServerRequestInterface $request, ResponseInterface $response, $args) : ResponseInterface {
        $type = strtolower(ArrayUtils::get($args, 'type'));
        $category = null;
        if ($type !== null) {
            switch ($type) {
                case 'actes-administratifs':
                    $category = 0;
                    break;
                case 'commande-publique':
                    $category = 1;
                    break;
                case 'urbanisme':
                    $category = 2;
                    break;
                case 'domaine-patrimoine':
                    $category = 3;
                    break;
                case 'finances-locales':
                    $category = 4;
                    break;
                case 'autres':
                    $category = 5;
                    break;
                default:
                    throw new \Exception('La cat&eacute;gorie de document "'.$type.'" n\'existe pas', 400);
            }
        }

        return $this->twig->render($response, 'accueil.html', [
            'currentUrl'        => $request->getUri()->getPath(),
            'category'          => $category
        ]);
    }
}