<?php
/**
 * @license Apache 2.0
 */

namespace DataSearchEngine\Lib;

use CURLFile;
use DateTime;
use SolrQuery;
use SolrObject;
use SolrDisMaxQuery;
use SolrUtils;
use SolrClient;
use DataSearchEngine\Entity\CitizenDocument;
use DataSearchEngine\Entity\DocumentFile;
use DataSearchEngine\Entity\SearchCriteria;

/**
 * SolrConsumer class to interact with Solr server.
 *
 * @package DataSearchEngine\Lib
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
class SolrConsumer {

	/** Solr API method */
	const INDEX_FILE = '/update/extract';

	const QUERY_STRING = '*:*';

	/** Solr API url for cURL */
	private $api;

	public function __construct() {
		$this->api = 'http://'.SOLR_SERVER.':'.SOLR_PORT.'/solr/'.SOLR_CORE;
	}
	
	/**
	 * Index a document in Solr with custom field values. 
	 * Complex document must be indexed through cURL request, this functionnality is not implemented yet in PHP Solr client.
	 *
	 * @param CitizenDocument $document Document to index with its metadata
	 * @param DocumentFile $file File to index with its open data URL
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return bool
	 * @throws Exception If curl call generate an error
	 * @access public
	 */
	public function indexDocumentFile(CitizenDocument $document, DocumentFile $file) : bool {
		$cFile = new CURLFile($file->getPath(), $file->getFormat(), $file->getName());

		// Basic configuration for writing in Solr index server
		$postConfiguration = array(
			'commitWithin'  => '1000',
			'overwrite'     => 'true',
			'wt'            => 'json',
			'commit'        => 'true'
		);
		
		$ch = curl_init();
		$options = array(
			CURLOPT_URL             => $this->api.self::INDEX_FILE,
			CURLOPT_ENCODING		=> '',
			CURLOPT_HTTP_VERSION    => CURL_HTTP_VERSION_1_1,
			CURLOPT_HEADER          => 0,
			CURLINFO_HEADER_OUT		=> true,
			CURLOPT_POST            => 1,
			CURLOPT_HTTPHEADER      => array('Content-Type: multipart/form-data', 'Authorization: Basic '.base64_encode(SOLR_USER.':'.SOLR_PASSWORD)),
			CURLOPT_POSTFIELDS      => array_merge($postConfiguration, $this->setMetadataArray($document, $file), array('myFile' => $cFile))
		);
		curl_setopt_array($ch, $options);
		curl_exec($ch);
		$result = false;
		$errmsg = null;
		if (curl_errno($ch) == 0) {
			$status = curl_getinfo($ch)['http_code'];
			if ($status == 200) {
				$result = true;
			} else if ($status == 401) {
				throw new \Exception('Vous n\'êtes pas autorisé à accéder au serveur d\'indexation, veuillez vérifier vos identifiants de connexion');
			}
		} else {
			$errmsg = curl_error($ch);
		}
		curl_close($ch);

		// Only if curl generate an error, throw exception.
		if (!$result && isset($errmsg)) {
			throw new \Exception($errmsg);
		}
		
		return $result;
	}

	/**
	 * Execute an admin search in Solr server
	 *
	 * @param SearchCriteria $criteria Document search criteria
	 * @param int $offset Start offset for search
	 * @param int $limit Limit for results display
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return SolrObject
	 * @throws \Exception If an error occured during request
	 * @access public
	 */
	public function adminSearch(SearchCriteria $criteria, int $offset, int $limit) : SolrObject {
		try {
			$queryString = self::QUERY_STRING;
			if (!empty($criteria->getFilename()) && strpos($criteria->getFilename(), '*') !== false) {
				$queryString = 'filepath:'.$criteria->getFilename();
			}
			$query = new SolrQuery($queryString);
			$query->setTimeAllowed(1500);
			$query->addSortField('date', 1);

			if (!empty($criteria->getSiren())) {
				$query->addFilterQuery('siren:'.$criteria->getSiren());
			}
			if (!empty($criteria->getFilename()) && strcmp($queryString, self::QUERY_STRING) == 0) {
				$query->addFilterQuery('filepath:"'.$criteria->getFilename().'"');
			}
			if (!empty($criteria->getDate())) {
				$query->addFilterQuery('date:'.$criteria->getDate().'T00\:00\:00Z');
			}
			$query->setStart($offset);
			$query->setRows($limit);

			$this->setFieldsForAdminResponse($query);
	
			return $this->getSolrClient()->query($query)->getResponse();

		} catch (\Exception $e) {
			if (strpos($e->getMessage(), '403')) {
				throw new \Exception('Vous n\'êtes pas authorisé à accèder à cette ressource', 403);
			} else {
				throw new \Exception($e->getMessage(), 500);
			}
		}
	}
	
	/**
	 * Execute an admin search in Solr server with siren and origin
	 *
	 * @param string $siren Entity SIREN to filter results for entity only
	 * @param int $total Total rows to limit search
	 * @param string $origin (optional) Origin of files
	 * @author Alexis ZUCHER <a.zucher@girondenumerique.fr>
	 * @return SolrObject
	 * @throws \Exception If an error occured during request
	 * @access public
	 */
	public function adminSearchByOrigin(string $siren, int $total, ?string $origin = null) : SolrObject {
		$query = new SolrQuery(self::QUERY_STRING);
		$query->setStart(0);
		$query->setRows($total);

		$query->addFilterQuery('siren:'.$siren);
		if($origin != null) {
			$query->addFilterQuery('origin:'.$origin);
		}

		$this->setFieldsForAdminResponse($query);

		return $this->getSolrClient()->query($query)->getResponse();
	}

	/**
	 * Execute a simple search in Solr server
	 *
	 * @param SearchCriteria $criteria Document search criteria
	 * @param int $offset Start offset for search
	 * @param int $limit Limit for results display
	 * @param boolean $qf (optional) Flag to activate queryField search for numerized documents (false by default)
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return SolrObject
	 * @throws \Exception If an error occured during request
	 * @access public
	 */
	public function simpleSearch(SearchCriteria $criteria, $offset, $limit, $qf = false) : SolrObject {
		try {
			$query = $this->initializeQuery($criteria, $offset, $limit, $qf);
	
			return $this->getSolrClient()->query($query)->getResponse();
		} catch (\Exception $e) {
			if (strpos($e->getMessage(), '403')) {
				throw new \Exception('Vous n\'êtes pas authorisé à accèder à cette ressource', 403);
			} else {
				throw new \Exception($e->getMessage(), 500);
			}
		}
	}
	
	 /**
	 * Execute an advanced search in Solr server
	 *
	 * @param SearchCriteria $criteria Document search criteria
	 * @param int $offset Start offset for search
	 * @param int $limit Limit for results display
	 * @param bool $qf (optional) Flag to activate queryField search for numerized documents (false by default)
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return SolrObject
	 * @throws \Exception If an error occured during request
	 * @access public
	 */
	public function advancedSearch(SearchCriteria $criteria, int $offset, int $limit, $qf = false) : SolrObject {
		try {
			if (!empty($criteria->getKeywords())) {
				$searchWords = $criteria->getKeywords();
			}
			$query = $this->initializeQuery($criteria, $offset, $limit, $qf);
			if (!empty($criteria->getFilename())) {
				$query->addFilterQuery('filepath:"'.$criteria->getFilename().'"');
			}
			if (!empty($criteria->getType())) {
				$query->addFilterQuery('documenttype:'.SolrUtils::queryPhrase($criteria->getType()));
			}
			if (!empty($criteria->getStartDate()) || !empty($criteria->getEndDate())) {
				$query->addFilterQuery('date:'.$this->setDateFilter($criteria->getStartDate(), $criteria->getEndDate()));
			}
			if (!empty($criteria->getZipcode())) {
				$query->addFilterQuery('codepostal:'.$criteria->getZipcode());
			}
			if (!empty($criteria->getCity())) {
				$query->addFilterQuery('ville:'.$criteria->getCity());
			}
	
			return $this->getSolrClient()->query($query)->getResponse();

		} catch (\Exception $e) {
			if (strpos($e->getMessage(), '403')) {
				throw new \Exception('Vous n\'êtes pas authorisé à accèder à cette ressource', 403);
			} else {
				throw new \Exception($e->getMessage(), 500);
			}
		}
	}

	/**
	 * Check if a document has already been indexed in Solr with its file hash.
	 *
	 * @param string $siren Entity Siren as index filter
	 * @param string $hash File hash
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return bool
	 * @access public
	 */
	public function documentIsAlreadyIndexed(string $siren, string $hash) : bool {
		$response = $this->searchDocument('', array('siren' => $siren, 'hash' => $hash));

		if (isset($response) && $response['response']['numFound'] > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Search documents in Solr server from search criteria.
	 *
	 * @param string $keywords Optional search keywords
	 * @param array $criteria Optional search criterias field => value
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return SolrObject
	 */
	private function searchDocument(string $keywords, array $criterias) : SolrObject {
		$query = new SolrQuery();

		// Main keywords
		if (is_null($keywords) || empty($keywords)) {
			$queryString = self::QUERY_STRING;
		}
		$query->setQuery($queryString);
		
		// Criteria by filter field
		if (sizeof($criterias) > 1) {
			foreach($criterias as $field => $value){
				$query->addFilterQuery($field.':'.$value);
			}
		}

		return $this->getSolrClient()->query($query)->getResponse();
	}

	/**
	 * Delete a document index in Solr by its solr document id.
	 *
	 * @param string $solrDocumentId Solr document id
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return bool
	 * @access public
	 */
	public function deleteDocumentIndexById(string $solrDocumentId) : bool {
		$deleteResponse = $this->getSolrClient()->deleteById($solrDocumentId);
		$commitResponse = $this->getSolrClient()->commit();
		
		// Check delete and commit request
		if ($deleteResponse->getHttpStatus() == 200 
			&& $commitResponse->getHttpStatus() == 200) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Construct a date range filter for query.
	 *
	 * @param string $startDate Start date from search form
	 * @param string $endDate End date from search form
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return string
	 * @access protected
	 */
	private function setDateFilter(?string $startDate, ?string $endDate) : string {
		$filter = '[';
		if (empty($startDate)) {
			$filter .= '*';
		} else {
			$filter .= $startDate.'T00:00:00Z';
		}
		$filter .= ' TO ';
		if (empty($endDate)) {
			$filter .= '*';
		} else {
			$filter .= $endDate.'T00:00:00Z';
		}
		$filter .= ']';

		return $filter;
	}

	/**
	 * Set a metadata array with all document informations for Solr indexation.
	 * 
	 *
	 * @param CitizenDocument $document Document to index with its metadata
	 * @param DocumentFile $file File to index with its open data URL
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return array
	 */
	private function setMetadataArray(CitizenDocument $document, DocumentFile $file) : array {
		$insertionDate = new DateTime();
		$fieldValues = array(
			'literal.hash'					=> $file->getHash(),
			'literal.filepath'      		=> $file->getOpenDataUrl(),
			'literal.description'      		=> $document->getDescription(),
			'literal.creationdate'			=> $creationDate = $insertionDate->format('Y-m-d').'T00:00:00Z',
			'literal.date'					=> $date = $document->getDate()->format('Y-m-d').'T00:00:00Z',
			'literal.origin'				=> $document->getOrigin(),
			'literal.entity'        		=> $document->getCollectivite()->getName(),
			'literal.siren'         		=> $document->getCollectivite()->getSiren(),
			'literal.nic'         			=> $document->getCollectivite()->getNic(),
			'literal.adresse1'      		=> $document->getCollectivite()->getAdresse()->getNumero_voie()
			.' '.$document->getCollectivite()->getAdresse()->getType_voie().' '.$document->getCollectivite()->getAdresse()->getLibelle_voie(),
			'literal.adresse2'      		=> $document->getCollectivite()->getAdresse()->getComplement(),
			'literal.ville'         		=> $document->getCollectivite()->getAdresse()->getCommune(),
			'literal.codepostal'    		=> $document->getCollectivite()->getAdresse()->getCode_postal(),
			'literal.boitepostale'    		=> $document->getCollectivite()->getAdresse()->getBoite_postale(),
			'literal.cedex'    				=> $document->getCollectivite()->getAdresse()->getCedex()
		);

		// These following values can be NULL, not necessary to save them as empty
		if (!empty($document->getIdentifier())) {
			$fieldValues['literal.documentIdentifier'] = $document->getIdentifier();
		}
		if (!empty($document->getType())) {
			$fieldValues['literal.documentType'] = $document->getType();
		}
		if (!empty($document->getClassification())) {
			$fieldValues['literal.classification'] = $document->getClassification();
		}
		if (!empty($file->getTypology())) {
			$fieldValues['literal.typology'] = $file->getTypology();
		}

		return $fieldValues;
	}
	
	/**
	 * Initialize a Solr Dismax query with field results and some technicals parameters.
	 *
	 * @param SearchCriteria $criteria Document search criteriay
	 * @param int $offset Start offset for search
	 * @param int $limit Limit for results display
	 * @param bool $qf (optional) Flag to activate queryField search for numerized documents (false by default)
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return SolrQuery
	 * @access protected
	 */
	protected function initializeQuery(SearchCriteria $criteria, int $offset, int $limit, $qf = false) : SolrQuery {
		$query = new SolrDisMaxQuery();
		// Search configuration
		$query->useDisMaxQueryParser();
		$query->setQueryAlt(self::QUERY_STRING);
		if (!empty($criteria->getKeywords())) {
			if ($qf) {
				$query->setQuery(SolrUtils::queryPhrase($criteria->getKeywords()));
			} else {
				$query->setQuery(SolrUtils::escapeQueryChars($criteria->getKeywords()));
			}
		}
		if (!empty($criteria->getSiren())) {
			$query->addFilterQuery('siren:'.$criteria->getSiren());
		}
		if (!empty($criteria->getFilename())) {
			$query->addFilterQuery('filepath:"'.$criteria->getFilename().'"');
		}
		if ($qf) {
			$query->addQueryField('entity OR adresse1 OR codepostal OR ville OR description OR classification');
		}
		$query->setMinimumMatch('95%');
		$query->setTimeAllowed(1500);
		$query->setMltMinWordLength(3);
		$query->addSortField('date', 1);
		$query->setStart($offset);
		$query->setRows($limit);

		$this->setFieldsForResponse($query);

		return $query;
	}

	/**
	 * Set fields for Solr response.
	 *
	 * @param SolrDisMaxQuery $query Query to be configured
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @access protected
	 */
	protected function setFieldsForResponse(SolrDisMaxQuery &$query) {
		$query->addField('entity');                 // Entity name
		$query->addField('adresse1');               // First line address
		$query->addField('adresse2');               // Address complement
		$query->addField('codepostal');             // Zipcode
		$query->addField('ville');                  // City
		$query->addField('boitepostale');           // Boite postale
		$query->addField('cedex');                  // Cedex
		$query->addField('siren');                  // Entity siren
		$query->addField('nic');                    // Entity nic : SIRET = siren+nic
		$query->addField('creationdate');           // Creation date in Solr database
		$query->addField('date');                   // Document date
		$query->addField('filepath');               // File OpenData URL
		$query->addField('documenttype');           // Document type
		$query->addField('classification');         // Classification
		$query->addField('stream_content_type');    // Type of document file
		$query->addField('id');                     // Solr document identifier
		$query->addField('documentidentifier');     // Pastell document identifier
		$query->addField('description');            // Document short description
		$query->addField('stream_name');            // Filename
	}

	/**
	 * Set fields for Solr response for administrator search.
	 *
	 * @param SolrQuery $query Query to be configured
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @access protected
	 */
	protected function setFieldsForAdminResponse(SolrQuery &$query) {
		$query->addField('id');                     // Solr document identifier
		$query->addField('description');            // Document short description
		$query->addField('siren');                  // Entity siren
		$query->addField('date');                   // Document date
		$query->addField('filepath');               // File OpenData URL
		$query->addField('stream_content_type');    // Type of document file
		$query->addField('stream_name');            // Filename
	}

	/**
	 * Get solr client
	 *
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return SolrClient
	 * @access protected
	 */
	protected function getSolrClient() {
		$config = array (
			'hostname' => SOLR_SERVER,
			'login'    => SOLR_USER,
			'password' => SOLR_PASSWORD,
			'port'     => SOLR_PORT,
			'timeout'  => 10,
			'path'     => '/solr/'.SOLR_CORE
		);

		$client = new SolrClient($config);
		$client->setResponseWriter('json');

		return $client;
	}
}