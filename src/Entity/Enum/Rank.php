<?php
/**
 * @license Apache 2.0
 */

namespace DataSearchEngine\Entity\Enum;

use MyCLabs\Enum\Enum;

/**
 * Enum class Rank
 * Implements MyCLabs\Enum\Enum, usefull to get all values for templates with function Enum::toArray()
 *
 * @package DataSearchEngine\Entity\Enum
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
class Rank extends Enum {

	/**
	 * Administrator rank
	 * @var string
	 */
	const ADMIN = 'ADMINISTRATOR';

	/**
	 * Agent rank
	 * @var string
	 */
    const AGENT = 'AGENT';

}