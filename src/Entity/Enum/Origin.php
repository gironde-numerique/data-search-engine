<?php
/**
 * @license Apache 2.0
 */

namespace DataSearchEngine\Entity\Enum;

use MyCLabs\Enum\Enum;

/**
 * Enum class Origin
 * Implements MyCLabs\Enum\Enum, usefull to get all values for templates with function Enum::toArray()
 *
 * @package DataSearchEngine\Entity\Enum
 * @author  Alexis ZUCHER <a.zucher@girondenumerique.fr>
 */
class Origin extends Enum {

	const TOUS                          = 'Tous les documents de la collectivit&eacute;';
    const ACTE_PASTELL                  = 'Documents d&eacute;vers&eacute;s depuis le flux &laquo; Acte &raquo; Pastell';
    const IMPORT_PASTELL                = 'Documents import&eacute;s depuis Pastell';
    const PUBLICATION_PASTELL           = 'Documents publi&eacute;s depuis Pastell';
    const PUBLICATION_ADMINISTRATION    = 'Documents import&eacute;s depuis cette interface';

}