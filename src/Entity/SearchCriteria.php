<?php
/**
 * @license Apache 2.0
 */

namespace DataSearchEngine\Entity;

/**
 * Class SearchCriteria to describe search criteria object.
 *
 * @package DataSearchEngine\Entity
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
class SearchCriteria {

    /**
	 * Keywords
	 * @var string
	 */
    private $keywords;

    /**
	 * Filename
	 * @var string
	 */
    private $filename;

    /**
	 * Document type
	 * @var string
	 */
    private $type;

    /**
	 * Organization siren
	 * @var string
	 */
    private $siren;

    /**
	 * Organization zipcode
	 * @var string
	 */
    private $zipcode;

    /**
	 * Organization city
	 * @var string
	 */
    private $city;

    /**
	 * Document date
	 * @var string
	 */
    private $date;

    /**
	 * Document start date interval
	 * @var string
	 */
    private $startDate;

    /**
	 * Document end date interval
	 * @var string
	 */
    private $endDate;

    /**
	 * Document origin
	 * @var string
	 */
    private $origin;

    /**
     * Constructor
     */
    public function __construct(?string $keywords, ?string $filename, ?string $type, ?string $siren) {
        $this->keywords   = $keywords;
        $this->filename   = $filename;
        $this->type       = $type;
        $this->siren      = $siren;
    }

    /**
     * Get keywords
     */ 
    public function getKeywords() {
        return $this->keywords;
    }

    /**
     * Set keywords
     *
     * @param  string  $keywords  Keywords
     * @return  self
     */ 
    public function setKeywords(string $keywords) {
        $this->keywords = $keywords;
        return $this;
    }

    /**
     * Get filename
     */ 
    public function getFilename() {
        return $this->filename;
    }

    /**
     * Set filename
     *
     * @param  string  $filename  Filename
     * @return  self
     */ 
    public function setFilename(string $filename) {
        $this->filename = $filename;
        return $this;
    }

    /**
     * Get document type
     */ 
    public function getType() {
        return $this->type;
    }

    /**
     * Set document type
     *
     * @param  string  $type  Document type
     * @return  self
     */ 
    public function setType(string $type) {
        $this->type = $type;
        return $this;
    }

    /**
     * Get organization siren
     */ 
    public function getSiren() {
        return $this->siren;
    }

    /**
     * Set organization siren
     *
     * @param  string  $siren  Organization siren
     * @return  self
     */ 
    public function setSiren(string $siren) {
        $this->siren = $siren;
        return $this;
    }

    /**
     * Get organization zipcode
     */ 
    public function getZipcode() {
        return $this->zipcode;
    }

    /**
     * Set organization zipcode
     *
     * @param  string  $zipcode  Organization zipcode
     * @return  self
     */ 
    public function setZipcode(string $zipcode) {
        $this->zipcode = $zipcode;
        return $this;
    }

    /**
     * Get organization city
     */ 
    public function getCity() {
        return $this->city;
    }

    /**
     * Set organization city
     *
     * @param  string  $city  Organization city
     * @return  self
     */ 
    public function setCity(string $city) {
        $this->city = $city;
        return $this;
    }

    /**
     * Get document date
     */ 
    public function getDate() {
        return $this->date;
    }

    /**
     * Set document date
     *
     * @param  string  $date  Document date
     * @return  self
     */ 
    public function setDate(string $date) {
        $this->date = $date;
        return $this;
    }

    /**
     * Get document start date interval
     */ 
    public function getStartDate() {
        return $this->startDate;
    }

    /**
     * Set document start date interval
     *
     * @param  string  $startDate  Document start date interval
     * @return  self
     */ 
    public function setStartDate(string $startDate) {
        $this->startDate = $startDate;
        return $this;
    }

    /**
     * Get document end date interval
     */ 
    public function getEndDate() {
        return $this->endDate;
    }

    /**
     * Set document end date interval
     *
     * @param  string  $endDate  Document end date interval
     * @return  self
     */ 
    public function setEndDate(string $endDate) {
        $this->endDate = $endDate;
        return $this;
    }
}