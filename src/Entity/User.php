<?php
/**
 * @license Apache 2.0
 */

namespace DataSearchEngine\Entity;

use DataSearchEngine\Entity\Enum\Rank;
use DataSearchEngine\Entity\Organization;

/**
 * Class User
 *
 * @package Data\Entity
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
class User implements \JsonSerializable {

	/**
	 * User identifier
	 * @var int
	 */
	private $id;

	/**
	 * User firstname
	 * @var string
	 */
	private $firstname;

	/**
	 * User lastname
	 * @var string
	 */
	private $lastname;

	/**
	 * User email
	 * @var string
	 */
	private $email;

	/**
	 * User rank
	 * @var Rank
	 */
	private $rank;

    /**
	 * Default constructor
	 */
	function __construct() {
		
	}

	/**
	 * Get user identifier
	 *
	 * @return  int
	 */ 
	public function getId() {
		return $this->id;
	}

	/**
	 * Set user identifier
	 *
	 * @param  int  $id  User identifier
	 * @return  self
	 */ 
	public function setId(int $id) {
		$this->id = $id;
		return $this;
	}

	/**
	 * Get user firstname
	 *
	 * @return  string
	 */ 
	public function getFirstname() {
		return $this->firstname;
	}

	/**
	 * Set user firstname
	 *
	 * @param  string  $firstname  User firstname
	 * @return  self
	 */ 
	public function setFirstname(string $firstname) {
		$this->firstname = $firstname;
		return $this;
	}

	/**
	 * Get user lastname
	 *
	 * @return  string
	 */ 
	public function getLastname() {
		return $this->lastname;
	}

	/**
	 * Set user lastname
	 *
	 * @param  string  $lastname  User lastname
	 * @return  self
	 */ 
	public function setLastname(string $lastname) {
		$this->lastname = $lastname;
		return $this;
	}

	/**
	 * Get user email
	 *
	 * @return  string
	 */ 
	public function getEmail() {
		return $this->email;
	}

	/**
	 * Set user email
	 *
	 * @param  string  $email  User email
	 * @return  self
	 */ 
	public function setEmail(string $email) {
		$this->email = $email;
		return $this;
	}

	/**
	 * Get user rank
	 *
	 * @return  Rank
	 */ 
	public function getRank() {
		return $this->rank;
	}

	/**
	 * Set user rank
	 *
	 * @param  Rank  $rank  User rank
	 * @return  self
	 */ 
	public function setRank(Rank $rank) {
		$this->rank = $rank;
		return $this;
	}
	
	/**
	 * To string
	 */
	public function __toString() {
		try {
			return $this->firstname.' '.$this->lastname.' ('.$this->email.')';
        } catch (\Exception $exception) {
            return '';
        }
	}

	/**
	 * JSON serialization
	 */
	public function jsonSerialize() {
        $vars = get_object_vars($this);
        return $vars;
    }
}