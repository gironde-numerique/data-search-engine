// Communauté de communes du Fronsadais
configuration = {
    limit: 10,
    siren: '243301397',
    //title: 'Communaut&eacute; de communes du Fronsadais',
    directory: 'OpenData',
    backgroundcolorstart: '#f4f4f4',
    backgroundcolorend: '#ffffff',
    gradient: true,
    color: '#e6eaea',
    image: '243301397.png'
}