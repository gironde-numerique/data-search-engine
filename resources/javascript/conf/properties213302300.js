// Mairie de Lapouyade
configuration = {
    limit: 10,
    siren: '213302300',
    //title: 'Mairie de Lapouyade',
    directory: 'OpenData',
    backgroundcolorstart: '#ffffff',
    //backgroundcolorend: '#e3e6e7',
    gradient: true,
    color: '#5e5e5e',
    image: '213302300.png'
}