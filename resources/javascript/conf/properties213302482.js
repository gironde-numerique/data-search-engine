// Mairie Listrac-Médoc
configuration = {
    limit: 10,
    siren: '213302482',
    //title: 'Listrac-Médoc',
    directory: 'OpenData',
    backgroundcolorstart: '#85ABFF',
    backgroundcolorend: '#ffffff',
    gradient: true,
    color: '#e6eaea',
    image: '213302482.png'
}