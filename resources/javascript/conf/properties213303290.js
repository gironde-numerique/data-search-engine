// Pompéjac
configuration = {
    limit: 10,
    siren: '213303290',
    title: 'Pomp&eacute;jac',
    directory: 'OpenData',
    backgroundcolorstart: '#f3f2f0',
    backgroundcolorend: '#ffffff',
    gradient: true,
    color: '#777777',
    image: '213303290.png'
}