// Communauté de communes Convergence Garonne
configuration = {
    limit: 10,
    siren: '200069581',
    //title: 'Communaut&eacute; de communes Convergence Garonne',
    directory: 'OpenData',
    backgroundcolorstart: '#555355',
    //backgroundcolorend: '#008d4f',
    gradient: true,
    color: '#ffffff',
    image: '200069581.png'
}