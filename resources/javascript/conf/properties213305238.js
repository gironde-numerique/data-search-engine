// Mairie de Targon
configuration = {
    limit: 10,
    siren: '213305238',
    //title: 'Mairie de Targon',
    directory: 'OpenData',
    backgroundcolorstart: '#074263',
    backgroundcolorend: '#e3e6e7',
    gradient: true,
    color: '#e6eaea',
    image: '213305238.png'
}