// Communauté de communes du secteur de Saint-Loubès
configuration = {
    limit: 10,
    siren: '243301249',
    //title: 'Communaut&eacute; de communes du secteur de Saint-Loub&egrave;s',
    directory: 'OpenData',
    backgroundcolorstart: '#b8003d',
    backgroundcolorend: '#ffffff',
    gradient: true,
    color: '#e6eaea',
    image: '243301249.png'
}