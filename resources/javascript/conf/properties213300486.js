// Mairie de Berthez
configuration = {
    limit: 10,
    siren: '213300486',
    title: 'Berthez',
    directory: 'OpenData',
    backgroundcolorstart: '#074263',
    backgroundcolorend: '#e3e6e7',
    gradient: true,
    color: '#e6eaea',
    image: '213300486.png'
}