// Communauté de communes du Bazadais
configuration = {
    limit: 10,
    siren: '200043982',
    //title: 'Communaut&eacute; de communes du Bazadais',
    directory: 'OpenData',
    backgroundcolorstart: '#87bec1',
    backgroundcolorend: '#a8cfd0',
    gradient: true,
    color: '#e6eaea',
    image: '200043982.png'
}