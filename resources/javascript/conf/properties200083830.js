// Mairie de Val-de-Livenne
configuration = {
    limit: 10,
    siren: '200083830',
    //title: 'Mairie de Val-de-Livenne',
    directory: 'OpenData',
    backgroundcolorstart: '#006080',
    backgroundcolorend: '#e6eaea',
    gradient: true,
    color: '#e6eaea',
    image: '200083830.png'
}