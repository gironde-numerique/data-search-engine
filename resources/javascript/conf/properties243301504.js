// Communauté d'agglomération du Bassin d'Arcachon Nord
configuration = {
    limit: 10,
    siren: '243301504',
    //title: 'Communaut&eacute; d'agglom&eacute;ration du Bassin d'Arcachon Nord',
    directory: 'OpenData',
    backgroundcolorstart: '#42a2c4',
    backgroundcolorend: '#008d4f',
    gradient: true,
    color: '#e6eaea',
    image: '243301504.png'
}