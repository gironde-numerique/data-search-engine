// Communauté de communes du Sud Gironde
configuration = {
    limit: 10,
    siren: '200043974',
    //title: 'Communaut&eacute; de communes du Sud Gironde',
    directory: 'OpenData',
    backgroundcolorstart: '#f3f6f7',
    backgroundcolorend: '#ffffff',
    gradient: true,
    color: '#e6eaea',
    image: '200043974.png'
}