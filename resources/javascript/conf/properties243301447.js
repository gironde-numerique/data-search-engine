// Communauté de communes Médoc Estuaire
configuration = {
    limit: 10,
    siren: '243301447',
    //title: 'Communaut&eacute; de communes M&eacute;doc Estuaire',
    directory: 'OpenData',
    backgroundcolorstart: '#84bddb',
    backgroundcolorend: '#dceaf2',
    gradient: true,
    color: '#e6eaea',
    image: '243301447.png'
}