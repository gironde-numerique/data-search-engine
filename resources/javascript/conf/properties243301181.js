// Communauté de communes Latitude Nord Gironde
configuration = {
    limit: 10,
    siren: '243301181',
    //title: 'Communaut&eacute; de communes Latitude Nord Gironde',
    directory: 'OpenData',
    backgroundcolorstart: '#ece111',
    backgroundcolorend: '#ffffff',
    gradient: true,
    color: '#e6eaea',
    image: '243301181.png'
}