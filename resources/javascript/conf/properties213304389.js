// Saint-Maixant
configuration = {
    limit: 10,
    siren: '213304389',
    //title: 'Saint-Maixant',
    directory: 'OpenData',
    backgroundcolorstart: '#074263',
    backgroundcolorend: '#e3e6e7',
    gradient: true,
    color: '#e6eaea',
    image: '213304389.png'
}