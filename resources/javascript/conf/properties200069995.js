// Communauté de communes Médoc Coeur de presqu'île
configuration = {
    limit: 10,
    siren: '200069995',
    //title: 'Communaut&eacute; de communes M&eacute;doc C&oelig;ur de presqu'&icirc;le',
    directory: 'OpenData',
    backgroundcolorstart: '#5c5450',
    backgroundcolorend: '#ffffff',
    gradient: true,
    color: '#e6eaea',
    image: '200069995.png'
}