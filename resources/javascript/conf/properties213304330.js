// Saint-Loubès
configuration = {
    limit: 10,
    siren: '213304330',
    //title: 'Saint-Loub&egrave;s',
    directory: 'OpenData',
    backgroundcolorstart: '#c0d79f',
    backgroundcolorend: '#e6e6e6',
    gradient: true,
    color: '#e6eaea',
    image: '213304330.png'
}