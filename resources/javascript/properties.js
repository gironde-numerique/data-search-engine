// // Gironde Numérique
// Saint-Loubès
// configuration = {
//     limit: 10,
//     siren: '213304330',
//     //title: 'Saint-Loub&egrave;s',
//     directory: 'OpenData',
//     backgroundcolorstart: '#c0d79f',
//     backgroundcolorend: '#e6e6e6',
//     gradient: true,
//     color: '#e6eaea',
//     image: '213304330.png'
// }

// Communauté de communes du Sud Gironde
configuration = {
    limit: 10,
    siren: '200043974',
    //title: 'Communaut&eacute; de communes du Sud Gironde',
    directory: 'OpenData',
    backgroundcolorstart: '#f3f6f7',
    backgroundcolorend: '#ffffff',
    gradient: true,
    color: '#e6eaea',
    image: '200043974.png'
}