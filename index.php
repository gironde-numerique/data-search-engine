<?php

/**
 * @license Apache 2.0
 */

define('DIR_ROOT', dirname(__FILE__));
require_once DIR_ROOT.DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'autoload.php'; 
require_once DIR_ROOT.DIRECTORY_SEPARATOR.'src'.DIRECTORY_SEPARATOR.'inc.config.php';

use DataSearchEngine\Lib\SolrConsumer;
use DataSearchEngine\Lib\SireneConsumer;
use DataSearchEngine\Middleware\EntryMiddleware;
use DataSearchEngine\Middleware\AuthenticationMiddleware;
use DI\Container;
use Odan\Session\PhpSession;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;
use Slim\Csrf\Guard;
use Slim\Exception\HttpBadRequestException;
use Slim\Factory\AppFactory;
use Slim\Flash\Messages;
use Slim\Routing\RouteCollectorProxy;

// Initialize data search engine application
$container = new Container();
AppFactory::setContainer($container);
$app = AppFactory::create();

// Container initialization
$responseFactory = $app->getResponseFactory();
$container->set('session', function () {
    return new PhpSession();
});
$container->set('csrf', function () use ($responseFactory) {
    $guard = new Guard($responseFactory);
    $guard->setPersistentTokenMode(true);
    $guard->setFailureHandler(function (Request $request) {
        throw new HttpBadRequestException($request);
    });
    return $guard;
});
$container->set('view', function() {
    if (!DEVELOPMENT) {
        return Twig::create('templates', ['cache' => 'templates/cache']);
    } else {
        return Twig::create('templates');
    }
});
$container->set('flash', function () {
    return new Messages();
});
$container->set('solr', function () {
    return new SolrConsumer();
});
$container->set('sirene', function () {
    return new SireneConsumer();
});
$container->set('user', function() {
    return null;
});

$app->add(TwigMiddleware::createFromContainer($app));
$app->add(new EntryMiddleware($container));

// Routing
$app->group('', function (RouteCollectorProxy $group) use ($app) {
    $group->group('', function (RouteCollectorProxy $group) use ($app) {
        $group->group('', function (RouteCollectorProxy $group) {
            $group->post('/action/admin-search',    \DataSearchEngine\Controller\Action\AdministrationAction::class.':search');
            $group->post('/action/admin-purge',     \DataSearchEngine\Controller\Action\AdministrationAction::class.':purge');
            $group->post('/action/upload',          \DataSearchEngine\Controller\Action\AdministrationAction::class.':upload');
            $group->post('/action/delete-document', \DataSearchEngine\Controller\Action\AdministrationAction::class.':delete');
        })->add(new AuthenticationMiddleware($app->getContainer()));
        $group->get('/',                            \DataSearchEngine\Controller\View\PublicViewController::class.':classicView');
        $group->get('/document/{type}',             \DataSearchEngine\Controller\View\PublicViewController::class.':withDocumentTypeFilter');
        $group->get('/administration',              \DataSearchEngine\Controller\View\AdministrationViewController::class);
        $group->post('/action/login',               \DataSearchEngine\Controller\Action\AuthenticationAction::class.':login');
        $group->post('/action/logout',              \DataSearchEngine\Controller\Action\AuthenticationAction::class.':logout');
    })->add('csrf');
    $group->post('/action/search',                  \DataSearchEngine\Controller\Action\PublicAction::class.':search');
    $group->post('/action/advanced-search',         \DataSearchEngine\Controller\Action\PublicAction::class.':advancedSearch');
    $group->post('/action/explore-directory',       \DataSearchEngine\Controller\Action\PublicAction::class.':explore');
});

// Define Custom Error Handler
$errorHandler = function (Request $request, Throwable $exception) use ($app) {
    // Display custom error page
    $request = $request->withAttribute('exception', $exception);
    return $app->get($request->getUri()->getPath(), DataSearchEngine\Controller\View\ErrorViewController::class)->run($request);
};
$errorMiddleware = $app->addErrorMiddleware(DEVELOPMENT, LOGGING, DEBUG);
if (PRODUCTION) {
    $errorMiddleware->setDefaultErrorHandler($errorHandler);
}

$app->run();